package pe.edu.tecsup.rutrux.`interface`

import pe.edu.tecsup.rutrux.model.Linea

interface IFirebaseLoad2 {
    fun onFirebaseLoadSuccess2(lineaList:List<Linea>)
    fun onFirebaseLoadFailed2(message:String)
}