package pe.edu.tecsup.rutrux.activitys

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.bottom_sheet.*
import pe.edu.tecsup.rutrux.R
import pe.edu.tecsup.rutrux.`interface`.IFirebaseLoad2
import pe.edu.tecsup.rutrux.`interface`.IFirebaseLoadDone
import pe.edu.tecsup.rutrux.fragments.DetallesFragment
import pe.edu.tecsup.rutrux.model.Empresa
import pe.edu.tecsup.rutrux.model.Linea

class MapsFragment : Fragment(), IFirebaseLoadDone, IFirebaseLoad2 {

    lateinit var detallesFragment: DetallesFragment
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    /*lateinit var txt1: Bundle
    var txt2: String? = null
    var txt3: String? = null
    var txt4: String? = null
    var txt5: String? = null*/

    lateinit var empRef2: DatabaseReference
    lateinit var empreRef: DatabaseReference
    lateinit var iFirebaseLoadDone: IFirebaseLoadDone
    lateinit var iFirebaseLoad2: IFirebaseLoad2
    lateinit var lineaRef: DatabaseReference


    @SuppressLint("MissingPermission")
    val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         **/
        googleMap.isMyLocationEnabled = true
        val coordinates = LatLng(-8.110466, -79.025566)
        googleMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 16f),
            400,
            null
        )

        googleMap.uiSettings.isZoomControlsEnabled = true
        try {
            val succes = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context,
            R.raw.uber_maps_style))
            if (!succes)
                Log.e("EDMT_ERROR", "Error de estilo")
        }catch (e:Resources.NotFoundException){
            Log.e("EDMT_ERROR", "ERROR")
        }
        val database = FirebaseDatabase.getInstance().getReference("value")
        val map = HashMap<String, Int>()
        map["value"] = 0
        database.updateChildren(map as Map<String, Int>)

        buttondet.visibility = View.GONE

        buttonmap.setOnClickListener() {
            createPolylines(googleMap)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            buttondet.visibility = View.VISIBLE
        }

        buttondet.setOnClickListener() {
            detallesFragment = DetallesFragment()
            fragmentManager?.beginTransaction()?.replace(R.id.frame_layout, detallesFragment)?.commit()
        }
    }

    private fun createPolylines(googleMap: GoogleMap) {
        val polylineOptions = PolylineOptions()
            .add(LatLng(-8.075790349458662, -79.03516173362732))
            .add(LatLng(-8.076448940159668, -79.03663158416747))
            .add(LatLng(-8.08763419552324, -79.03037667274475))
            .add(LatLng(-8.086954378787992, -79.02378916740417))
            .add(LatLng(-8.089153156899272, -79.02353167533875))
            .add(LatLng(-8.08913191272368, -79.02378916740417))
            .add(LatLng(-8.093741872584161, -79.02508735656737))
            .add(LatLng(-8.095600712445762, -79.01940107345581))
            .add(LatLng(-8.097618871725818, -79.01622533798216))
            .add(LatLng(-8.101612672907656, -79.01174068450928))
            .add(LatLng(-8.104820431331195, -79.01030302047728))
            .add(LatLng(-8.111299600468852, -79.0133285522461))
            .add(LatLng(-8.123131780800188, -79.0226411819458))
            .add(LatLng(-8.12687042238651, -79.03030157089233))
            .add(LatLng(-8.124639986994742, -79.03839111328125))
            .add(LatLng(-8.1241726561038, -79.03849840164185))
            .add(LatLng(-8.124363836988591, -79.03909921646118))
            .add(LatLng(-8.124767440779662, -79.03907775878906))
            .add(LatLng(-8.124363836988591, -79.05510663986206))
            .add(LatLng(-8.1435398788139, -79.04771447181702))
            .add(LatLng(-8.148770514610845, -79.04672205448149))
            .add(LatLng(-8.150607858777432, -79.04481768608093))
            .add(LatLng(-8.14855810433652, -79.04325664043425))
            .add(LatLng(-8.147352673894224, -79.04476404190063))

        val polylineOptions2 = PolylineOptions()
            .add(LatLng(-8.076448940159668, -79.0365993976593))
            .add(LatLng(-8.07704379535339, -79.03797268867493))
            .add(LatLng(-8.07984810088912, -79.03641700744629))
            .add(LatLng(-8.083417188844027, -79.04285430908202))
            .add(LatLng(-8.08436256697157, -79.04394865036011))
            .add(LatLng(-8.094878421404152, -79.03931379318237))
            .add(LatLng(-8.095876882207623, -79.04109477996826))
            .add(LatLng(-8.097502031201191, -79.04316544532776))
            .add(LatLng(-8.10144272472832, -79.04451727867126))
            .add(LatLng(-8.10234557360898, -79.04061198234558))
            .add(LatLng(-8.104831053005354, -79.03565526008606))
            .add(LatLng(-8.108962862978524, -79.03729677200317))
            .add(LatLng(-8.109971910378974, -79.03813362121582))
            .add(LatLng(-8.11264852910955, -79.03449654579163))
            .add(LatLng(-8.119180678552532, -79.04027938842773))
            .add(LatLng(-8.119255027609642, -79.04069781303406))
            .add(LatLng(-8.11978609190299, -79.04074072837828))
            .add(LatLng(-8.119881683401209, -79.04035449028015))
            .add(LatLng(-8.124268246557575, -79.03891682624817))
            .add(LatLng(-8.124778061926573, -79.03897047042847))
            .add(LatLng(-8.124756819632458, -79.03819799423218))
            .add(LatLng(-8.126711105983958, -79.03255462646484))
            .add(LatLng(-8.126711105983958, -79.03043031692505))
            .add(LatLng(-8.126381851884899, -79.02835965156555))
            .add(LatLng(-8.12514980189153, -79.02536630630493))
            .add(LatLng(-8.124077065627253, -79.02355313301086))
            .add(LatLng(-8.124247004236476, -79.02296304702759))
            .add(LatLng(-8.123758430541004, -79.02255535125732))
            .add(LatLng(-8.123195507937156, -79.02270555496216))
            .add(LatLng(-8.119881683401209, -79.01919722557068))
            .add(LatLng(-8.114825924056651, -79.01512026786804))
            .add(LatLng(-8.110502986941059, -79.01295304298401))
            .add(LatLng(-8.105022243092227, -79.01033520698547))
            .add(LatLng(-8.104395564134963, -79.01033520698547))
            .add(LatLng(-8.102749199512274, -79.01113986968994))
            .add(LatLng(-8.098489863659227, -79.00748133659363))
            .add(LatLng(-8.097321459414633, -79.00973439216614))
            .add(LatLng(-8.096864718651975, -79.01086091995239))
            .add(LatLng(-8.093741872584161, -79.00882244110107))
            .add(LatLng(-8.090884553445715, -79.01337146759033))
            .add(LatLng(-8.086635714298813, -79.01105403900146))
            .add(LatLng(-8.086040873245265, -79.00990605354309))
            .add(LatLng(-8.08561598624125, -79.01073217391968))
            .add(LatLng(-8.085414164757555, -79.01448726654053))
            .add(LatLng(-8.086965000933304, -79.02378916740417))
            .add(LatLng(-8.08074037583735, -79.02458310127258))
            .add(LatLng(-8.081452069917418, -79.03212547302246))
            .add(LatLng(-8.082131895926196, -79.03356313705444))
            .add(LatLng(-8.076448940159668, -79.0365993976593))

        val polylineOptions3 = PolylineOptions()
            .add(LatLng(-8.076087777650205, -79.03595566749573))
            .add(LatLng(-8.080612908108382, -79.03350949287415))
            .add(LatLng(-8.07965689885785, -79.03187870979309))
            .add(LatLng(-8.079625031843827, -79.02785539627075))
            .add(LatLng(-8.079380717986108, -79.02474403381348))
            .add(LatLng(-8.086922512350412, -79.02379989624022))
            .add(LatLng(-8.085456653499362, -79.01457309722899))
            .add(LatLng(-8.08561598624125, -79.01082873344421))
            .add(LatLng(-8.086040873245265, -79.00980949401855))
            .add(LatLng(-8.086667580759064, -79.01116132736206))
            .add(LatLng(-8.090863309361392, -79.01342511177062))
            .add(LatLng(-8.093710006683178, -79.00889754295349))
            .add(LatLng(-8.096939071834685, -79.01101112365723))
            .add(LatLng(-8.097576384266246, -79.00957345962524))
            .add(LatLng(-8.098585460220113, -79.00765299797058))
            .add(LatLng(-8.102812929881054, -79.0112578868866))
            .add(LatLng(-8.111724460371578, -79.01899337768555))
            .add(LatLng(-8.114262988932754, -79.02097821235655))
            .add(LatLng(-8.11884079697334, -79.0257203578949))
            .add(LatLng(-8.116419132396878, -79.02834892272949))
            .add(LatLng(-8.116429753764608, -79.02992606163025))
            .add(LatLng(-8.115516315113057, -79.03146028518677))
            .add(LatLng(-8.113689431580417, -79.0327262878418))
            .add(LatLng(-8.113073387586521, -79.0327799320221))
            .add(LatLng(-8.112138698344403, -79.03411030769348))
            .add(LatLng(-8.119244406316616, -79.04027938842773))
            .add(LatLng(-8.11935061923428, -79.04077291488647))
            .add(LatLng(-8.119881683401209, -79.04062271118164))
            .add(LatLng(-8.11995603232854, -79.04026865959167))
            .add(LatLng(-8.124300110037112, -79.03880953788757))
            .add(LatLng(-8.124979863664558, -79.03923869132996))
            .add(LatLng(-8.134018353205827, -79.04706001281738))
            .add(LatLng(-8.135792039932008, -79.0429937839508))
            .add(LatLng(-8.136493015755594, -79.04329419136047))
            .add(LatLng(-8.146487098253317, -79.05168414115906))
            .add(LatLng(-8.149758220905222, -79.04773592948914))
            .add(LatLng(-8.148759894099802, -79.04671669006348))
            .add(LatLng(-8.150618479239418, -79.0447747707367))
            .add(LatLng(-8.148632447945365, -79.04328346252441))
            .add(LatLng(-8.1475703950798, -79.04447436332703))
        val polylineOptions4 = PolylineOptions()
            .add(LatLng(-8.07792545411762, -79.02491569519043))
            .add(LatLng(-8.083427811082494, -79.02425050735474))
            .add(LatLng(-8.083427811082494, -79.02339220046997))
            .add(LatLng(-8.086848157319583, -79.02307033538818))
            .add(LatLng(-8.088930092999748, -79.02281284332274))
            .add(LatLng(-8.08955679602273, -79.02244806289673))
            .add(LatLng(-8.09117134847468, -79.02200818061829))
            .add(LatLng(-8.091426277217968, -79.0226411819458))
            .add(LatLng(-8.091782114985847, -79.02282893657684))
            .add(LatLng(-8.09238225856908, -79.02182042598724))
            .add(LatLng(-8.092631875548475, -79.02351021766663))
            .add(LatLng(-8.09303020019536, -79.02443289756775))
            .add(LatLng(-8.093125798052004, -79.02493715286255))
            .add(LatLng(-8.093301060730305, -79.02502298355103))
            .add(LatLng(-8.093816226343302, -79.02503907680511))
            .add(LatLng(-8.093943689898467, -79.02377307415009))
            .add(LatLng(-8.096174295585566, -79.01815116405487))
            .add(LatLng(-8.097634804522007, -79.01819407939911))
            .add(LatLng(-8.098840384271497, -79.0186071395874))
            .add(LatLng(-8.100035338610837, -79.01940643787384))
            .add(LatLng(-8.104204373750289, -79.02302205562592))
            .add(LatLng(-8.105101905601622, -79.02261435985565))
            .add(LatLng(-8.106148138427576, -79.02234613895416))
            .add(LatLng(-8.106535828073142, -79.02297914028168))
            .add(LatLng(-8.105601123634626, -79.02412712574005))
            .add(LatLng(-8.105165635597768, -79.02627825737))
            .add(LatLng(-8.10623842235101, -79.02901411056519))
            .add(LatLng(-8.103296218182512, -79.03220057487488))
            .add(LatLng(-8.102876660239748, -79.0330159664154))
            .add(LatLng(-8.105144392266844, -79.03424978256226))
            .add(LatLng(-8.10532496054399, -79.03478622436523))
            .add(LatLng(-8.105059418931909, -79.03552651405334))
            .add(LatLng(-8.105277163066752, -79.0358644723892))
            .add(LatLng(-8.109069078613604, -79.03747916221619))
            .add(LatLng(-8.109982531917094, -79.03816044330596))
            .add(LatLng(-8.112595421768354, -79.03453946113586))
            .add(LatLng(-8.1191966104945, -79.04024183750153))
            .add(LatLng(-8.119244406316616, -79.04066562652588))
            .add(LatLng(-8.11973829614535, -79.0407782793045))
            .add(LatLng(-8.11992416850423, -79.04036521911621))
            .add(LatLng(-8.124273557137679, -79.0389060974121))
            .add(LatLng(-8.124921447382416, -79.03920114040375))
            .add(LatLng(-8.144612562995132, -79.05623853206635))
            .add(LatLng(-8.14477187228188, -79.05614733695984))
            .add(LatLng(-8.125043590508113, -79.03915286064148))
            .add(LatLng(-8.124703713892266, -79.03847694396973))
            .add(LatLng(-8.124284178297655, -79.03839111328125))
            .add(LatLng(-8.12412486086837, -79.03863787651062))
            .add(LatLng(-8.123896505776095, -79.03891682624817))
            .add(LatLng(-8.119828577016113, -79.04023647308348))
            .add(LatLng(-8.119488695985236, -79.04007554054259))
            .add(LatLng(-8.119281580840997, -79.04010772705078))
            .add(LatLng(-8.110800389509414, -79.03282821178436))
            .add(LatLng(-8.108569864884807, -79.03565526008606))
            .add(LatLng(-8.108012231795447, -79.03682470321655))
            .add(LatLng(-8.106190624982306, -79.03607904911041))
            .add(LatLng(-8.106615490282813, -79.03485596179962))
            .add(LatLng(-8.102993499204452, -79.03298377990721))
            .add(LatLng(-8.103386502745911, -79.03223276138306))
            .add(LatLng(-8.10632339543692, -79.02901411056519))
            .add(LatLng(-8.10521874392019, -79.02624607086182))
            .add(LatLng(-8.10538337967512, -79.02558624744415))
            .add(LatLng(-8.103221866173909, -79.02383208274841))
            .add(LatLng(-8.104262793044123, -79.02303814888))
            .add(LatLng(-8.10520812225626, -79.02256608009338))
            .add(LatLng(-8.106142827607927, -79.02232468128204))
            .add(LatLng(-8.10315282501077, -79.01968538761139))
            .add(LatLng(-8.10211189526942, -79.0190577507019))
            .add(LatLng(-8.100497386671188, -79.01742160320282))
            .add(LatLng(-8.097650737317547, -79.01627898216248))
            .add(LatLng(-8.096126497021785, -79.01800096035004))
            .add(LatLng(-8.093879958125928, -79.02384281158447))
            .add(LatLng(-8.09373656160084, -79.0248030424118))
            .add(LatLng(-8.093263883804909, -79.02478158473969))
            .add(LatLng(-8.092977090265181, -79.0248566865921))
            .add(LatLng(-8.08913191272368, -79.02380526065826))
            .add(LatLng(-8.089185023160546, -79.02355313301086))
            .add(LatLng(-8.086954378787992, -79.0237784385681))
            .add(LatLng(-8.086826913022545, -79.02302742004395))
            .add(LatLng(-8.083454366677408, -79.02344584465027))
            .add(LatLng(-8.083401255485821, -79.02428805828094))
            .add(LatLng(-8.07792545411762, -79.02491569519043))
        val polylineOptions5 = PolylineOptions()
            .add(LatLng(-8.078684953798433, -79.02481377124786))
            .add(LatLng(-8.083427811082494, -79.02427732944489))
            .add(LatLng(-8.083470300033529, -79.02341365814209))
            .add(LatLng(-8.086933134496551, -79.02307033538818))
            .add(LatLng(-8.085446031314317, -79.01446580886841))
            .add(LatLng(-8.09264780854191, -79.01813507080078))
            .add(LatLng(-8.095643200113749, -79.01940107345581))
            .add(LatLng(-8.098696989512458, -79.0207314491272))
            .add(LatLng(-8.100667335249632, -79.02184188365936))
            .add(LatLng(-8.10314751415158, -79.02386963367462))
            .add(LatLng(-8.104278725577345, -79.02299523353577))
            .add(LatLng(-8.105181568095228, -79.02259826660156))
            .add(LatLng(-8.106551760516341, -79.02223348617554))
            .add(LatLng(-8.10696069300903, -79.02201890945435))
            .add(LatLng(-8.107151882083775, -79.02230322360992))
            .add(LatLng(-8.105648921073378, -79.02407884597778))
            .add(LatLng(-8.105144392266844, -79.02626752853394))
            .add(LatLng(-8.106227800714013, -79.0289980173111))
            .add(LatLng(-8.10696069300903, -79.02201890945435))
            .add(LatLng(-8.107151882083775, -79.02230322360992))
            .add(LatLng(-8.105648921073378, -79.02407884597778))
            .add(LatLng(-8.105144392266844, -79.02626752853394))
            .add(LatLng(-8.106227800714013, -79.0289980173111))
            .add(LatLng(-8.103328083324842, -79.03216302394867))
            .add(LatLng(-8.102876660239748, -79.03302669525146))
            .add(LatLng(-8.105155013932452, -79.03424978256226))
            .add(LatLng(-8.105346203865386, -79.0347808599472))
            .add(LatLng(-8.10504879726378, -79.03549432754517))
            .add(LatLng(-8.105255919741705, -79.03585910797119))
            .add(LatLng(-8.109106254079252, -79.0375006198883))
            .add(LatLng(-8.109934734993361, -79.03818726539612))
            .add(LatLng(-8.112611353971447, -79.03455018997192))
            .add(LatLng(-8.119217853082816, -79.0402740240097))
            .add(LatLng(-8.119308134070574, -79.04069781303406))
            .add(LatLng(-8.11976484934475, -79.040687084198))
            .add(LatLng(-8.131862304282496, -79.05117988586426))
            .add(LatLng(-8.134092699513628, -79.04656648635864))
            .add(LatLng(-8.135770798221245, -79.04302597045898))
            .add(LatLng(-8.13623811559796, -79.04309034347534))
            .add(LatLng(-8.143423051650325, -79.04934525489807))
            .add(LatLng(-8.144224910132566, -79.04997289180756))
            .add(LatLng(-8.145350695490135, -79.05080437660217))
            .add(LatLng(-8.146471167395752, -79.05172169208527))
            .add(LatLng(-8.147437638273429, -79.0525209903717))
            .add(LatLng(-8.14454883926268, -79.05593276023865))
            .add(LatLng(-8.133909488944548, -79.04681861400604))
            .add(LatLng(-8.13217562245688, -79.05045837163925))
            .add(LatLng(-8.131748128954706, -79.05100017786026))
            .add(LatLng(-8.119828577016113, -79.04061198234558))
            .add(LatLng(-8.119701121663244, -79.04009699821472))
            .add(LatLng(-8.119276270194854, -79.04008626937866))
            .add(LatLng(-8.11078976799291, -79.03285503387451))
            .add(LatLng(-8.108561898703257, -79.03565257787704))
            .add(LatLng(-8.107993644012485, -79.0367978811264))
            .add(LatLng(-8.106209212848587, -79.03608977794647))
            .add(LatLng(-8.106180003344049, -79.03594762086868))
            .add(LatLng(-8.106642044349192, -79.03478622436523))
            .add(LatLng(-8.105016932257717, -79.0340593457222))
            .add(LatLng(-8.102993499204452, -79.03295427560806))
            .add(LatLng(-8.103442266730834, -79.03218448162079))
            .add(LatLng(-8.104645173667116, -79.03079777956009))
            .add(LatLng(-8.10637119278985, -79.02901411056519))
            .add(LatLng(-8.1062437331694, -79.02883440256117))
            .add(LatLng(-8.105226710167946, -79.0262433886528))
            .add(LatLng(-8.105370102600615, -79.02553796768188))
            .add(LatLng(-8.10068326792519, -79.02172118425369))
            .add(LatLng(-8.099780415316054, -79.02114450931549))
            .add(LatLng(-8.085448686860614, -79.01437193155287))
            .add(LatLng(-8.086051495414623, -79.01805996894836))
            .add(LatLng(-8.085392920384962, -79.01837110519409))
            .add(LatLng(-8.084118255982368, -79.0187519788742))
            .add(LatLng(-8.08074037583735, -79.01958346366882))
            .add(LatLng(-8.080018058174216, -79.01975512504578))
            .add(LatLng(-8.080411084121845, -79.02221202850342))
            .add(LatLng(-8.078445950556464, -79.0225875377655))
            .add(LatLng(-8.078684953798433, -79.02481377124786))


        val value: String = spinner.selectedItem as String
        val value2: String = spinner2.selectedItem as String
        val resultado = (value.equals("Señor de los Milagros") && value2.equals("B1"))
        val resultado2 = (value.equals("Señor de los Milagros") && value2.equals("A1"))
        val resultado3 = (value.equals("Señor de los Milagros") && value2.equals("A"))
        val resultado4 = (value.equals("Virgen de la Puerta") && value2.equals("B"))
        val resultado5 = (value.equals("Virgen de la Puerta") && value2.equals("B♥"))
        if (resultado) {
            googleMap.clear()
            googleMap.addPolyline(polylineOptions)
            val database = FirebaseDatabase.getInstance().getReference("value")
            val map = HashMap<String, Int>()
            map["value"] = 1
            database.updateChildren(map as Map<String, Int>)
        } else if (resultado2) {
            googleMap.clear()
            googleMap.addPolyline(polylineOptions2)
            val database = FirebaseDatabase.getInstance().getReference("value")
            val map = HashMap<String, Int>()
            map["value"] = 2
            database.updateChildren(map as Map<String, Int>)
        } else if (resultado3) {
            googleMap.clear()
            googleMap.addPolyline(polylineOptions3)
            val database = FirebaseDatabase.getInstance().getReference("value")
            val map = HashMap<String, Int>()
            map["value"] = 3
            database.updateChildren(map as Map<String, Int>)
        } else if (resultado4) {
            googleMap.clear()
            googleMap.addPolyline(polylineOptions4)
            val database = FirebaseDatabase.getInstance().getReference("value")
            val map = HashMap<String, Int>()
            map["value"] = 4
            database.updateChildren(map as Map<String, Int>)
        } else if (resultado5) {
            googleMap.clear()
            googleMap.addPolyline(polylineOptions5)
            val database = FirebaseDatabase.getInstance().getReference("value")
            val map = HashMap<String, Int>()
            map["value"] = 5
            database.updateChildren(map as Map<String, Int>)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        iFirebaseLoad2 = this
        iFirebaseLoadDone = this

        empreRef = FirebaseDatabase.getInstance().getReference("empresa")

        empreRef.addValueEventListener(object : ValueEventListener {
            var empresaList: MutableList<Empresa> = ArrayList<Empresa>()
            override fun onDataChange(p0: DataSnapshot) {
                for (empresaSnapShot in p0.children)
                    empresaList.add(empresaSnapShot.getValue<Empresa>(Empresa::class.java)!!)
                iFirebaseLoadDone.onFirebaseLoadSuccess(empresaList)
            }

            override fun onCancelled(p0: DatabaseError) {
                iFirebaseLoadDone.onFirebaseLoadFailed(p0.message)
            }

        })

        bottomSheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        textView3.setOnClickListener { expandCollapseSheet() }


        spinner.onItemSelectedListener = object : OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    buttondet.visibility = View.GONE
                    lineaRef = FirebaseDatabase.getInstance().getReference("linea/0")
                    lineaRef.addValueEventListener(object : ValueEventListener {
                        var lineaList: MutableList<Linea> = ArrayList<Linea>()
                        override fun onDataChange(snapshot: DataSnapshot) {
                            for (lineaSnapShot in snapshot.children)
                                lineaList.add(lineaSnapShot.getValue<Linea>(Linea::class.java)!!)
                            iFirebaseLoad2.onFirebaseLoadSuccess2(lineaList)
                        }

                        override fun onCancelled(error: DatabaseError) {
                            (iFirebaseLoad2 as MapsFragment).onFirebaseLoadFailed(error.message)
                        }

                    })
                } else {
                    buttondet.visibility = View.GONE
                    lineaRef = FirebaseDatabase.getInstance().getReference("linea/1")
                    lineaRef.addValueEventListener(object : ValueEventListener {
                        var lineaList: MutableList<Linea> = ArrayList<Linea>()
                        override fun onDataChange(snapshot: DataSnapshot) {
                            for (lineaSnapShot in snapshot.children)
                                lineaList.add(lineaSnapShot.getValue<Linea>(Linea::class.java)!!)
                            iFirebaseLoad2.onFirebaseLoadSuccess2(lineaList)
                        }

                        override fun onCancelled(error: DatabaseError) {
                            (iFirebaseLoad2 as MapsFragment).onFirebaseLoadFailed(error.message)
                        }

                    })
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }

    }

    private fun expandCollapseSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        } else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        }
    }

    override fun onFirebaseLoadSuccess(empresaList: List<Empresa>) {
        val empresa_name_list = getEmpresaNameList(empresaList)

        val adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            empresa_name_list
        )
        spinner.adapter = adapter
    }

    private fun getEmpresaNameList(empresaList: List<Empresa>): List<String> {
        val result = ArrayList<String>()
        for (empresa in empresaList)
            result.add(empresa.nombre)
        return result
    }

    override fun onFirebaseLoadSuccess2(lineaList: List<Linea>) {
        val linea_name_list = getLineaNameList(lineaList)
        val adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            linea_name_list
        )
        spinner2.adapter = adapter
    }

    private fun getLineaNameList(lineaList: List<Linea>): List<String> {
        val result = ArrayList<String>()
        for (linea in lineaList)
            result.add(linea.nombre!!)
        return result
    }

    override fun onFirebaseLoadFailed(message: String) {
    }

    override fun onFirebaseLoadFailed2(message: String) {
    }



}
