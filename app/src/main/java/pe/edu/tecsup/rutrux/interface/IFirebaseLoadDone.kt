package pe.edu.tecsup.rutrux.`interface`

import pe.edu.tecsup.rutrux.model.Empresa

interface IFirebaseLoadDone {
    fun onFirebaseLoadSuccess(empresaList:List<Empresa>)
    fun onFirebaseLoadFailed(message:String)
}