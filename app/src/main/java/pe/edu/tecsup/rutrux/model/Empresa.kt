package pe.edu.tecsup.rutrux.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Empresa {
    var nombre =""
    var distrito = ""
    var pasajecom =""
    var pasajeesc =""
    var pasajeur =""
}