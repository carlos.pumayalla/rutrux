package pe.edu.tecsup.rutrux.activitys

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import pe.edu.tecsup.rutrux.R
import pe.edu.tecsup.rutrux.R.id.*
import pe.edu.tecsup.rutrux.fragments.AboutFragment

class   HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var empRef2:DatabaseReference
    lateinit var mapFragment2: MapsFragment
    lateinit var aboutFragment: AboutFragment
    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(drawer_layout)
        navView = findViewById(nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        showMap()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            nav_ruta -> {
                mapFragment2 = MapsFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, mapFragment2)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }

            nav_about -> {
                aboutFragment = AboutFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, aboutFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            nav_logout -> {
                FirebaseAuth.getInstance().signOut()
                onBackPressed()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showMap(){
        mapFragment2 = MapsFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout, mapFragment2)
            .commit()
    }

}
