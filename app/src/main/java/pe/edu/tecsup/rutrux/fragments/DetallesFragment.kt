package pe.edu.tecsup.rutrux.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_detalles.*
import pe.edu.tecsup.rutrux.R
import pe.edu.tecsup.rutrux.activitys.MapsFragment
import pe.edu.tecsup.rutrux.model.Empresa
import pe.edu.tecsup.rutrux.model.Linea
import pe.edu.tecsup.rutrux.model.Value



class DetallesFragment : Fragment() {

    lateinit var mapsFragment: MapsFragment
    lateinit var empRef2 : DatabaseReference
    lateinit var empRef3 : DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

            var database = FirebaseDatabase.getInstance().getReference("value")
            var getData = object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val value = snapshot.getValue(Value::class.java)
                    if (value?.value == 1){
                        empRef2 = FirebaseDatabase.getInstance().getReference("empresa/0")
                        val listener = object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val empre = snapshot.getValue(Empresa::class.java)
                                textnom.text = empre?.nombre
                                textdis.text = empre?.distrito
                                textcom.text = empre?.pasajecom
                                textesc.text = empre?.pasajeesc
                                textur.text = empre?.pasajeur
                            }

                            override fun onCancelled(error: DatabaseError) {
                                TODO("Not yet implemented")
                            }
                        }
                        empRef2.addValueEventListener(listener)
                        empRef3 = FirebaseDatabase.getInstance().getReference("linea/0/0")
                        val listener2 = object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val linea = snapshot.getValue(Linea::class.java)
                                textli.text = linea?.nombre
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        }
                        empRef3.addValueEventListener(listener2)
                    }else if(value?.value == 2) {
                        empRef2 = FirebaseDatabase.getInstance().getReference("empresa/0")
                        val listener = object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val empre = snapshot.getValue(Empresa::class.java)
                                textnom.text = empre?.nombre
                                textdis.text = empre?.distrito
                                textcom.text = empre?.pasajecom
                                textesc.text = empre?.pasajeesc
                                textur.text = empre?.pasajeur
                            }

                            override fun onCancelled(error: DatabaseError) {
                            }
                        }
                        empRef2.addValueEventListener(listener)
                        empRef3 = FirebaseDatabase.getInstance().getReference("linea/0/1")
                        val listener2 = object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val linea = snapshot.getValue(Linea::class.java)
                                textli.text = linea?.nombre
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        }
                        empRef3.addValueEventListener(listener2)
                    }else if (value?.value == 3){
                        empRef2 = FirebaseDatabase.getInstance().getReference("empresa/0")
                        val listener = object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val empre = snapshot.getValue(Empresa::class.java)
                                textnom.text = empre?.nombre
                                textdis.text = empre?.distrito
                                textcom.text = empre?.pasajecom
                                textesc.text = empre?.pasajeesc
                                textur.text = empre?.pasajeur
                            }

                            override fun onCancelled(error: DatabaseError) {
                            }
                        }
                        empRef2.addValueEventListener(listener)
                        empRef3 = FirebaseDatabase.getInstance().getReference("linea/0/2")
                        val listener2 = object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val linea = snapshot.getValue(Linea::class.java)
                                textli.text = linea?.nombre
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        }
                        empRef3.addValueEventListener(listener2)
                    }else if(value?.value == 4){
                        empRef2 = FirebaseDatabase.getInstance().getReference("empresa/1")
                        val listener = object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val empre = snapshot.getValue(Empresa::class.java)
                                textnom.text = empre?.nombre
                                textdis.text = empre?.distrito
                                textcom.text = empre?.pasajecom
                                textesc.text = empre?.pasajeesc
                                textur.text = empre?.pasajeur
                            }

                            override fun onCancelled(error: DatabaseError) {
                            }
                        }
                        empRef2.addValueEventListener(listener)
                        empRef3 = FirebaseDatabase.getInstance().getReference("linea/1/0")
                        val listener2 = object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val linea = snapshot.getValue(Linea::class.java)
                                textli.text = linea?.nombre
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        }
                        empRef3.addValueEventListener(listener2)
                    }else{
                        empRef2 = FirebaseDatabase.getInstance().getReference("empresa/1")
                        val listener = object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val empre = snapshot.getValue(Empresa::class.java)
                                textnom.text = empre?.nombre
                                textdis.text = empre?.distrito
                                textcom.text = empre?.pasajecom
                                textesc.text = empre?.pasajeesc
                                textur.text = empre?.pasajeur
                            }

                            override fun onCancelled(error: DatabaseError) {
                            }
                        }
                        empRef2.addValueEventListener(listener)
                        empRef3 = FirebaseDatabase.getInstance().getReference("linea/1/1")
                        val listener2 = object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val linea = snapshot.getValue(Linea::class.java)
                                textli.text = linea?.nombre
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        }
                        empRef3.addValueEventListener(listener2)
                    }

                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }
            database.addValueEventListener(getData)
        return inflater.inflate(R.layout.fragment_detalles, container, false)
    }
}